// Set the total time for the quiz in seconds
var totalTime = 180; // 180 seconds = 3 minutes

// Display the countdown timer
function displayTimer() {
    var minutes = Math.floor(totalTime / 60);
    var seconds = totalTime % 60;
    document.getElementById("timer").textContent =
        minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
}

// Update the countdown timer every second
function startTimer() {
    displayTimer();
    var timerInterval = setInterval(function () {
        totalTime--;
        displayTimer();
        if (totalTime <= 0) {
            clearInterval(timerInterval);
            // Time's up, submit the form
            document.getElementById("quizForm").submit();
        }
    }, 1000);
}

// Call startTimer when the page loads
window.onload = startTimer;
