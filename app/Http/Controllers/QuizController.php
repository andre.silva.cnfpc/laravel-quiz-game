<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QuizController extends Controller
{
    public function index()
    {
        $questions = json_decode(file_get_contents(storage_path('app/public/questions.json')), true);
        return view('quiz.index', compact('questions'));
    }

    public function submit(Request $request)
    {
        $answers = $request->input('answers', []);
        $questions = json_decode(file_get_contents(storage_path('app/public/questions.json')), true);
        $score = 0;

        foreach ($questions as $index => $question) {
            if (isset($answers[$index]) && $answers[$index] === $question['answer']) {
                $score++;
            }
        }

        return view('quiz.result', compact('score'));
    }
}
