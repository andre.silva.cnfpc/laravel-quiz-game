@extends('layouts.app')

@section('title', 'Quiz Results')

@section('content')
    <h2>Your Score: {{ $score }}</h2>
    <a href="{{ route('quiz.index') }}">Try Again</a>
@endsection
