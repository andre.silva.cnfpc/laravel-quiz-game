@extends('layouts.app')

@section('title', 'Quiz')

@section('content')
    <div id="timer"></div>
    <form id="quizForm" action="{{ route('quiz.submit') }}" method="POST">
        @csrf

        @foreach ($questions as $index => $question)
            <div class="question">
                <p>{{ $question['text'] }}</p>
                <input type="radio" name="answers[{{ $index }}]" value="True"> True
                <input type="radio" name="answers[{{ $index }}]" value="False"> Falseeee
            </div>
        @endforeach

        <button type="submit">Submit Answers</button>
    </form>
@endsection
