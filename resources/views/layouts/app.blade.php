<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quiz Game - @yield('title', 'Home')</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    <header>
        <h1>Quiz Game</h1>
    </header>

    <main class="container">

        @yield('content')
    </main>

    <footer>
        <p>&copy; {{ date('Y') }} Quiz Game</p>
    </footer>

    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
